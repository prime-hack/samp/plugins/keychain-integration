[![pipeline status](https://gitlab.com/prime-hack/samp/plugins/keychain-integration/badges/main/pipeline.svg)](https://gitlab.com/prime-hack/samp/plugins/keychain-integration/-/commits/main)



## keychain-integration

SAMP plugin to integrate password dialogs with Credential Vault.

![demo](demo.png)

When checkbox is checked, plugin save input data after close dialog. When checkbox unchecked, stored data was removed.

All passwords stored in Windows Credential Vault



#### Supported SAMP versions

- 0.3.7-R1
- 0.3.7-R3
- 0.3.7-R3-1
