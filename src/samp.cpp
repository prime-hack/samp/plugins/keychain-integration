#include "samp.h"

#include <windows.h>

#include "sampapi/CDialog.h"
#include "sampapi/CNetGame.h"
#include "sampapi/CPlayerPool.h"
#include "sampapi/sampapi.h"

samp_ver get_samp_ver() {
	static constexpr auto kR1Entry = 0x31DF13;
	static constexpr auto kR3Entry = 0xCC490;
	static constexpr auto kR3_1Entry = 0xCC4D0;
	auto base = sampapi::GetBase();
	auto *ntheader = reinterpret_cast<IMAGE_NT_HEADERS *>( base + reinterpret_cast<IMAGE_DOS_HEADER *>( base )->e_lfanew );
	auto ep = ntheader->OptionalHeader.AddressOfEntryPoint;
	switch ( ep ) {
		case kR1Entry:
			return samp_ver::r1;
		case kR3Entry:
			[[fallthrough]];
		case kR3_1Entry:
			return samp_ver::r3;
	}
	return samp_ver::unk;
}

void RemoveControl( CDXUTDialog *dialog, int ID ) {
	static constexpr auto kR1Addr = 0x828D0;
	static constexpr auto kR3Addr = 0x867E0;

	auto addr = get_samp_ver() == samp_ver::r1 ? kR1Addr : kR3Addr;
	auto func = reinterpret_cast<void( __thiscall * )( CDXUTDialog *, int )>( sampapi::GetAddress( addr ) );
	return func( dialog, ID );
}
int AddCheckBox( CDXUTDialog *dialog,
				 int ID,
				 const char *strText,
				 int x,
				 int y,
				 int w,
				 int h,
				 bool bChecked,
				 int nHotKey,
				 bool bIsDefault,
				 CDXUTCheckBox **ppCreated ) {
	static constexpr auto kR1Addr = 0x8C750;
	static constexpr auto kR3Addr = 0x906A0;

	auto addr = get_samp_ver() == samp_ver::r1 ? kR1Addr : kR3Addr;
	auto func =
		reinterpret_cast<int( __thiscall * )( CDXUTDialog *, int, const char *, int, int, int, int, bool, int, bool, CDXUTCheckBox ** )>(
			sampapi::GetAddress( addr ) );
	return func( dialog, ID, strText, x, y, w, h, bChecked, nHotKey, bIsDefault, ppCreated );
}
void SetText( CDXUTEditBox *edit, const char *lpString, bool select ) {
	static constexpr auto kR1Addr = 0x80F60;
	static constexpr auto kR3Addr = 0x84E70;

	auto addr = get_samp_ver() == samp_ver::r1 ? kR1Addr : kR3Addr;
	auto func = reinterpret_cast<void( __thiscall * )( CDXUTEditBox *, const char *, bool )>( sampapi::GetAddress( addr ) );
	return func( edit, lpString, select );
}

CDXUTDialog *dialog() {
	if ( get_samp_ver() == samp_ver::r1 ) {
		if ( sampapi::v037r1::RefDialog() == nullptr ) return nullptr;
		return sampapi::v037r1::RefDialog()->m_pDialog;
	}
	if ( sampapi::v037r3::RefDialog() == nullptr ) return nullptr;
	return sampapi::v037r3::RefDialog()->m_pDialog;
}
CDXUTEditBox *edit() {
	if ( get_samp_ver() == samp_ver::r1 ) {
		if ( sampapi::v037r1::RefDialog() == nullptr ) return nullptr;
		return reinterpret_cast<CDXUTEditBox *>( sampapi::v037r1::RefDialog()->m_pEditbox );
	}
	if ( sampapi::v037r3::RefDialog() == nullptr ) return nullptr;
	return reinterpret_cast<CDXUTEditBox *>( sampapi::v037r3::RefDialog()->m_pEditbox );
}

bool is_active() {
	if ( get_samp_ver() == samp_ver::r1 ) {
		if ( sampapi::v037r1::RefDialog() == nullptr ) return false;
		return sampapi::v037r1::RefDialog()->m_bIsActive != FALSE;
	}
	if ( sampapi::v037r3::RefDialog() == nullptr ) return false;
	return sampapi::v037r3::RefDialog()->m_bIsActive != FALSE;
}
bool is_passwd() {
	if ( get_samp_ver() == samp_ver::r1 ) {
		if ( sampapi::v037r1::RefDialog() == nullptr ) return false;
		return sampapi::v037r1::RefDialog()->m_nType == sampapi::v037r1::CDialog::DIALOG_PASSWORD;
	}
	if ( sampapi::v037r3::RefDialog() == nullptr ) return false;
	return sampapi::v037r3::RefDialog()->m_nType == sampapi::v037r3::CDialog::DIALOG_PASSWORD;
}

std::string host() {
	using namespace std::string_literals;

	const char *ip;
	int port;
	if ( get_samp_ver() == samp_ver::r1 ) {
		if ( sampapi::v037r1::RefNetGame() == nullptr ) return "";
		ip = sampapi::v037r1::RefNetGame()->m_szHostAddress;
		port = sampapi::v037r1::RefNetGame()->m_nPort;
	} else {
		if ( sampapi::v037r3::RefNetGame() == nullptr ) return "";
		ip = sampapi::v037r3::RefNetGame()->m_szHostAddress;
		port = sampapi::v037r3::RefNetGame()->m_nPort;
	}

	return ip + ":"s + std::to_string( port );
}
std::string name() {
	using namespace std::string_literals;

	const char *name;
	unsigned int id;
	if ( get_samp_ver() == samp_ver::r1 ) {
		if ( sampapi::v037r1::RefDialog() == nullptr || sampapi::v037r1::RefNetGame() == nullptr ||
			 sampapi::v037r1::RefNetGame()->GetPlayerPool() == nullptr )
			return "";
		name = sampapi::v037r1::RefNetGame()->GetPlayerPool()->GetLocalPlayerName();
		id = sampapi::v037r1::RefDialog()->m_nId;
	} else {
		if ( sampapi::v037r3::RefDialog() == nullptr || sampapi::v037r3::RefNetGame() == nullptr ||
			 sampapi::v037r3::RefNetGame()->GetPlayerPool() == nullptr )
			return "";
		name = sampapi::v037r3::RefNetGame()->GetPlayerPool()->GetLocalPlayerName();
		id = sampapi::v037r3::RefDialog()->m_nId;
	}

	return name + "@"s + std::to_string( id );
}
const char *hostname() {
	if ( get_samp_ver() == samp_ver::r1 ) {
		if ( sampapi::v037r1::RefNetGame() == nullptr ) return "";
		return sampapi::v037r1::RefNetGame()->m_szHostname;
	}
	if ( sampapi::v037r3::RefNetGame() == nullptr ) return "";
	return sampapi::v037r3::RefNetGame()->m_szHostname;
}
