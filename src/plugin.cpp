#include "plugin.h"

#include <stdexcept>
#include <string>

#include "CDXUT/CheckBox.h"
#include "CDXUT/Dialog.h"
#include "CDXUT/EditBox.h"
#include "keychain/keychain.h"

#include "samp.h"

namespace {
	constexpr auto kCheckBoxID = 25;
	constexpr auto kLeftButtonID = 20;
	constexpr auto kRightButtonID = 21;
	constexpr auto kKeychainPackage = "SAMP";

} // namespace

plugin::plugin() {
	if ( get_samp_ver() == samp_ver::unk ) throw std::runtime_error( "SAMP API version not supported!" );

	on_mainloop = on_Render2dStuff.connect( &plugin::mainloop, this );
}

plugin::~plugin() {
	if ( keep_pass_checkbox == nullptr ) return;

	if ( dialog() != nullptr ) RemoveControl( dialog(), kCheckBoxID );
}

void plugin::mainloop() {
	keychain::Error error{};

	if ( !is_active() ) {
		auto dialog_closed = data_inserted && is_passwd();
		data_inserted = false;
		if ( !dialog_closed ) return;

		if ( !keep_pass_checkbox->GetChecked() ) {
			auto password = keychain::getPassword( kKeychainPackage, host(), name(), error );
			if ( error.type == keychain::ErrorType::NoError )
				keychain::deletePassword( kKeychainPackage, host(), name(), error );
			else if ( error.type == keychain::ErrorType::NotFound ) {
				password = keychain::getPassword( kKeychainPackage, hostname(), name(), error );
				if ( error.type == keychain::ErrorType::NoError ) keychain::deletePassword( kKeychainPackage, hostname(), name(), error );
			}
		} else
			keychain::setPassword( kKeychainPackage, host(), name(), edit()->Text(), error );
		return;
	}

	const auto *pos = edit()->controlData.pos;
	const auto *size = edit()->controlData.size;
	const auto height = size[1] / 2;
	if ( keep_pass_checkbox == nullptr ) {
		AddCheckBox( dialog(),
					 kCheckBoxID,
					 "Keep this password",
					 pos[0],
					 pos[1] + size[1],
					 size[0],
					 height,
					 false,
					 0,
					 false,
					 &keep_pass_checkbox );
	} else {
		keep_pass_checkbox->controlData.pos[0] = pos[0];
		keep_pass_checkbox->controlData.pos[1] = pos[1] + size[1];
		keep_pass_checkbox->controlData.size[0] = size[0];
		keep_pass_checkbox->controlData.size[1] = height;
		keep_pass_checkbox->UpdateRects();

		if ( lbtn == nullptr || rbtn == nullptr ) {
			for ( auto i = 0ull; i < dialog()->m_Controls.m_nSize; ++i ) {
				auto *ctrl = dialog()->m_Controls.m_pData[i];
				if ( ctrl->controlData.GetID() == kLeftButtonID ) lbtn = static_cast<CDXUTButton *>( ctrl );
				if ( ctrl->controlData.GetID() == kRightButtonID ) rbtn = static_cast<CDXUTButton *>( ctrl );
			}
		}
	}

	bool show = edit()->GetEnabled() && edit()->GetVisible() && is_passwd();
	keep_pass_checkbox->SetVisible( show );
	keep_pass_checkbox->SetEnabled( show );

	if ( show && lbtn != nullptr && rbtn != nullptr ) {
		auto offset = static_cast<int>( height * 1.5f );
		lbtn->controlData.pos[1] = pos[1] + size[1] + offset;
		lbtn->UpdateRects();
		rbtn->controlData.pos[1] = pos[1] + size[1] + offset;
		rbtn->UpdateRects();

		dialog()->m_height = lbtn->controlData.pos[1] + lbtn->controlData.size[1] + offset;
	}

	if ( !show || data_inserted ) return;
	data_inserted = true;

	auto password = keychain::getPassword( kKeychainPackage, host(), name(), error );
	if ( error.type == keychain::ErrorType::NotFound ) password = keychain::getPassword( kKeychainPackage, hostname(), name(), error );
	keep_pass_checkbox->SetChecked( !password.empty() );

	if ( !keep_pass_checkbox->GetChecked() ) return;

	if ( error.type != keychain::ErrorType::NoError ) return;

	SetText( edit(), password.c_str(), true );
}
