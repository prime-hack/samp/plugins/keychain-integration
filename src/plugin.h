#pragma once

#include "sigslot/signal.hpp"

extern sigslot::signal_st<> on_Render2dStuff;

class CDXUTCheckBox;
class CDXUTButton;

struct plugin {
	plugin();
	~plugin();

private:
	sigslot::scoped_connection on_mainloop;
	CDXUTCheckBox *keep_pass_checkbox = nullptr;
	CDXUTButton *lbtn = nullptr;
	CDXUTButton *rbtn = nullptr;
	bool data_inserted = false;

	void mainloop();
};
