#pragma once

#include <string>

class CDXUTDialog;
class CDXUTCheckBox;
class CDXUTEditBox;

enum class samp_ver {
	unk,
	r1,
	r3
};
samp_ver get_samp_ver();

void RemoveControl( CDXUTDialog *dialog, int ID );
int AddCheckBox( CDXUTDialog *dialog,
				 int ID,
				 const char *strText,
				 int x,
				 int y,
				 int w,
				 int h,
				 bool bChecked,
				 int nHotKey,
				 bool bIsDefault,
				 CDXUTCheckBox **ppCreated );
void SetText( CDXUTEditBox *edit, const char *lpString, bool select );

CDXUTDialog *dialog();
CDXUTEditBox *edit();

bool is_active();
bool is_passwd();

std::string host();
std::string name();
const char *hostname();
