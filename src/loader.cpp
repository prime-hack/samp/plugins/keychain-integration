#include <stdexcept>

#include "MinHook.h"
#include "sigslot/signal.hpp"

#include "plugin.h"

namespace address {
	struct address_t {
		std::uintptr_t value = 0;

		constexpr address_t( std::uintptr_t address ) : value( address ) {}

		template<typename T> operator T *() const { return reinterpret_cast<T *>( value ); }
	};
	consteval address_t operator""_addr( unsigned long long int value ) {
		return { static_cast<std::uintptr_t>( value ) };
	}

	static constexpr auto Render2dStuff = 0x0053E230_addr;
} // namespace address

sigslot::signal_st<> on_Render2dStuff;
namespace {
	void *oRender2dStuff;
	void __cdecl Render2dStuff() {
		on_Render2dStuff();
		reinterpret_cast<decltype( &Render2dStuff )>( oRender2dStuff )();
	}
} // namespace

class loader {
	struct plugin plugin;

public:
	loader() {
		if ( MH_Initialize() != MH_OK ) throw std::runtime_error( "MinHook not inicialized!" );
		if ( auto err = MH_CreateHook( address::Render2dStuff, reinterpret_cast<void *>( &Render2dStuff ), &oRender2dStuff );
			 err != MH_OK ) {
			MH_Uninitialize();
			throw std::runtime_error( "Hook not created!" );
		}

		MH_EnableHook( address::Render2dStuff );
	}
	~loader() {
		MH_DisableHook( address::Render2dStuff );
		MH_RemoveHook( address::Render2dStuff );
		MH_Uninitialize();
	}
} loader;
